import argparse
import random

class MPU:
    def __init__(self,IS,RAM):
        self.__IS = IS
        self.__RAM = RAM
        self.__PC = None
        self.__ACC = None
        self.__INREG = None
        self.__OUTREG = None
        self.__OVF = None
        self.__HALT = None

        self.__reset()

    def __reset(self):
        self.__PC = 0
        self.__ACC = 0
        self.__INREG = 0
        self.__OUTREG = 0
        self.__OVF = 0
        self.__HALT = 0

    def set_ir(self,value):
        self.__INREG = value

    def execute(self):
        self.__PC, self.__ACC, self.__OUTREG, self.__OVF, self.__HALT, self.__RAM = self.__IS.execute(self.__PC, self.__ACC, self.__INREG, self.__OUTREG, self.__RAM)
        return self.__HALT

    def dump(self):

        #print format(i,'02d'),format(m,'08b')
        print "PC  : " + format(self.__PC,'05b')
        print "ACC : " + str(self.__ACC)
        print "OVF : " + format(self.__OVF,'01b')
        print "HLT : " + format(self.__HALT,'01b')
        print "IN  : " + format(self.__INREG,'08b')
        print "OUT : " + format(self.__OUTREG,'08b')
        print "RAM : "
        self.__RAM.dump()

class IS:
    def __init__(self):
        self.__MAXV = 127
        self.__MINV = -128
        return

    def execute(self,PC,ACC,IR,OR,RAM):
        #d = RAM.read(PC)
        #op = (d & 0xE0) >> 5
        #arg = (d & 0x1F) >> 0
        #op,arg,val = self.__decode(d)
        op,arg,val = self.__decode(RAM.read(PC))
        print op,arg,val

        r_pc = PC+1
        r_or = OR
        r_acc = ACC
        r_ovf = 0
        r_hlt = 0

        if op == int('000',2):
            print "instruction : Jump"
            if arg == int('00000',2):
                r_hlt = 1
            else:
                if r_acc >= 0:
                    r_pc = arg

        elif op == int('001',2):
            print "instruction : Add"
            x,xx,v = self.__decode(RAM.read(arg))
            r_acc = ACC + v
            if r_acc > self.__MAXV:
                r_acc = self.__MAXV
                r_ovf = 1

        elif op == int('010',2):
            print "instruction : Sub"
            x,xx,v = self.__decode(RAM.read(arg))
            r_acc = ACC - v
            if r_acc < self.__MINV:
                r_acc = self.__MINV
                r_ovf = 1

        elif op == int('011',2):
            print "instruction : Load"
            x,xx,v = self.__decode(RAM.read(arg))
            r_acc = v

        elif op == int('100',2):
            print "instruction : Store"
            #RAM.write(arg,r_acc)
            RAM.write(arg,self.__encode(r_acc))

        elif op == int('101',2):
            print "instruction : Read"
            #RAM.write(arg,IR)
            RAM.write(arg,IR)

        elif op == int('110',2):
            print "instruction : Write"
            r_or = RAM.read(arg)

        elif op == int('111',2):
            print "instruction : Shift"
            r_acc = ACC << arg
            if r_acc > self.__MAXV:
                r_acc = self.__MAXV
                r_ovf = 1

        return r_pc,r_acc,r_or,r_ovf,r_hlt,RAM

    def __decode(self,d):
        op = (d & 0xE0) >> 5
        arg = (d & 0x1F) >> 0

        val = d
        if d > self.__MAXV:
           val = (~(d-(self.__MAXV+1))+1)

        return op,arg,val

    def __encode(self,d):
        val = d
        if d < 0:
            val = abs(d) + self.__MAXV+1

        return val

class RAM:
    def __init__(self,numbytes):
        self.__numbytes = numbytes
        self.__m = None
        self.__ac = None
        self.__reset()

    def __reset(self):
        self.__m = [random.randint(0,255) for x in range(0,self.__numbytes)]
        self.__ac = [0] * self.__numbytes

    def write(self,addr,value):
        if addr < 0 or addr >= self.__numbytes:
            print "RAM : Out of addr range"
            exit(-1)

        self.__m[addr] = value & 255
        self.__ac[addr] = 1

    def read(self,addr):
        if addr < 0 or addr >= self.__numbytes:
            print "RAM : Out of addr range"
            exit(-1)

        return self.__m[addr]

    def dump(self):
        for i,m in enumerate(self.__m):
            print format(i,'05b'),format(m,'08b')

    def count_ac(self):
        return sum(self.__ac)

class Loader:
    def __init__(self,RAM,F):
        self.__RAM = RAM
        self.__F = F

    def load(self):
        lines = self.__F.readlines()

        for i,l in enumerate(lines):
            #print i,int(l,2)
            self.__RAM.write(i,int(l.strip(),2))

def run():
    p = argparse.ArgumentParser()
    p.add_argument("dat",type=open,help='code/data binfile')
    p.add_argument("-i",type=str,help='input register')

    args = p.parse_args()

    R = RAM(32)
    #R.write(0,(1<<5)+ 10)

    I = IS()
    M = MPU(I,R)

    L = Loader(R,args.dat)
    L.load()

    if args.i is not None:
        M.set_ir(int(args.i,2))

    st = 1
    while True:
        print str(st) + ' --------------------'
        halt = M.execute()
        M.dump()

        print "execution step  : " + str(st)
        print "accessed ram ct : " + str(R.count_ac())

        if halt > 0:
            break
        st = st+1
   

if __name__ == "__main__":
    run()
